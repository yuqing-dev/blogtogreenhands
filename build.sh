#!/bin/bash

# 配置环境
npm install -g hexo-cli
if [ -d "hexo" ]; then
  rm -rf hexo
fi
# Vercel的部署会保留上一次部署的内容

# 安装默认配置
mkdir hexo
hexo init hexo

#安装 particlex 主题
mv ./particlex ./hexo/themes/particlex

# 复制配置文件
rm ./hexo/_config.yml
cp _config.yml ./hexo/_config.yml

# 复制文章
rm -rf ./hexo/source/_posts
cp -r  ./posts ./hexo/source/_posts

# 开始构建
cd ./hexo
hexo g

# 复制图片发挥pages的图床功能
cd ../
mv ./my_images ./hexo/public/my_images