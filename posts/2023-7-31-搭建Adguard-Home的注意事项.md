---
title: 搭建AdguardHome的注意事项
date: 2023/7/31 20:47:25
tags: DNS
categories: DNS
---

## DNS缓存设置

DNS缓存设置如图，具体原因需要讲解CDN原理
![DNS缓存设置](my_images/2023-7-31-搭建Adguard-Home的注意事项/DNS缓存设置.png "DNS缓存设置")  

## 上游 DNS 服务器设置

用我先前批量获取的DNS服务器在本地批量ping或者批量tracert，在结果中拿几个最快的扔进去就可以了。  
本地批量ping用pinginfoview的话不能ping ipv6地址，而批量tracert用Best Trace则没有这个问题。  

## 使用效果

![效果](my_images/2023-7-31-搭建Adguard-Home的注意事项/效果.png "效果")  
