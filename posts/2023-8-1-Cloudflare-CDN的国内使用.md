---
title: Cloudflare CDN的国内使用
date: 2023/8/1 12:46:25
tags: [DNS, 任播]
categories: Cloudflare
---

## Cloudflare CDN

众所周知，Cloudflare CDN 在国内的使用环境可谓差得不能再差，以至于优选节点都难以将其救回。  
其实Cloudflare CDN不是在国内没有节点，只是我们白嫖党不配用，如下图。
![Cloudflare CDN在国内有节点](/my_images/Cloudflare-CDN的国内使用/官网图片.png "Cloudflare CDN在国内有节点")  
那么，如何优化Cloudflare CDN的使用体验呢？  
我的回答是：优选IP+任播。  
没错，Cloudflare不仅在DNS领域上用了任播，也在CDN领域上用了任播(不知道为什么不用业内广泛使用的CNAME方案)，如下图。  
![ping官网](/my_images/Cloudflare-CDN的国内使用/ping官网.png "ping官网")  
![查询IP地址](/my_images/Cloudflare-CDN的国内使用/查询IP地址.png "查询IP地址")  
在我使用CloudflareST工具时，发现有些IP段延迟极低，如下图
![IP段延迟](/my_images/Cloudflare-CDN的国内使用/IP段延迟.png "IP段延迟")  
没错，Cloudflare在这一整个IP段上全部应用了任播技术。  
在我的广东联通家宽测试环境下，有两个IP段表现良好，如下。  

```none
172.64.229.0/24
162.159.58.0/24
```

看到这里，由于我与各位的网络环境不同，测试结果不能直接套用，但是相信各位能以此作为参考，提高Cloudflare CDN的使用体验。  
